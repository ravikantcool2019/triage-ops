# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../group_definition'

module Generate
  module GroupCiJob
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), nil, '-')
      policy_base = "policies/generated/#{template_name}"

      FileUtils.rm_rf(File.join(destination, "#{template_name}.yml"), secure: true)
      FileUtils.mkdir_p(destination)

      groups = GroupDefinition::DATA.each.
        each_with_object({}) do |(name, definition), result|
          next if definition['ignore_templates']&.include?(options.template)

          result[name] = "#{policy_base}/#{name}.yml"
        end

      if groups.any?
        File.write(
          "#{destination}/#{template_name}.yml",
          erb.result_with_hash(groups: groups)
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../.gitlab/ci")
    end
  end
end
